#+TITLE: Contact
#+AUTHOR: Matthias Paulmier
#+DATE:
#+STARTUP: showall
#+STARTUP: inlineimages entitiespretty


- Email              :: [[mailto:mpaulmier@gmx.fr][hello@mattplm.fr]]
- LinkedIn           :: [[https://www.linkedin.com/in/matthias-paulmier-599aa4170/][Matthias Paulmier]]
- Gitlab             :: [[https://gitlab.com/mattplm][mattplm]]
- Github             :: [[https://github.com/mpaulmier][mpaulmier]]
