(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-refresh-contents)
(package-install 'htmlize)
(package-install 'rainbow-delimiters)

(require 'htmlize)
(require 'org)
(require 'ox-publish)
(require 'rainbow-delimiters)

(with-eval-after-load 'rainbow-delimiters
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(setq org-html-htmlize-output-type 'css)

(setq org-export-html-coding-system 'utf8-unix
      org-html-viewport nil
      org-html-html5-fancy t
      org-html-head-include-default-style nil)

(setq mp/project-path
      (file-name-directory
       (or (buffer-file-name) load-file-name)))

(setq mp/head-extra
      (concat
       "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/res/style.css\"/>"
       "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/res/code.css\"/>"
       "<link rel=\"icon\" type=\"image/x-icon\" href=\"/static/res/favicon.ico\"/>"))

(setq mp/header-file "static/res/header.html")

(defun mp/header (arg)
  (with-temp-buffer
    (insert-file-contents (concat mp/project-path mp/header-file))
    (buffer-string)))

(setq mp/footer
      (concat
       "<a rel=\"license\" "
       "href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img "
       "alt=\"Licence Creative Commons\" style=\"border-width:0\" "
       "src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\""
       "/></a><br />"
       "© Matthias Paulmier -- "
       "Vous pouvez copier, modifier et/ou distribuer ce document "
       "sous les termes de la <a rel=\"license\" "
       "href=\"http://creativecommons.org/licenses/by-sa/4.0/\">Licence "
       "CC BY-SA</a>."))

(defun mp/org-export-format-drawer (name content)
  "Format org drawers."
  (concat "<div class=\"drawer " (downcase name) "\">\n"
          "<h6>" (capitalize name) "</h6>\n"
          content
          "\n</div>"))

(defun mp/blog-post-get-preview (filename)
  "Helper function that returns the content of the preview for
the sitemap. FILENAME is the file of the article."
  (with-temp-buffer
    (insert-file-contents (concat "blog/" filename))
    (goto-char (point-min))
    (let ((beg (+ 1 (re-search-forward "^#\\+BEGIN_PREVIEW$")))
          (end (progn (re-search-forward "^#\\+END_PREVIEW$")
                      (match-beginning 0))))
      (buffer-substring beg end))))

(defun mp/get-tags (filename)
  "FILENAME is the file from which we want to extract the tags
list. Returns the tags in the file."
  (with-temp-buffer
    (insert-file-contents (concat "blog/" filename))
    (org-mode)
    org-file-tags))

(defun mp/sitemap (title list)
  "Create the sitemap.  TITLE is the title of the sitemap.  LIST
holds the list of files to include in the sitemap"
  (concat "#+TITLE: " (capitalize title) "\n"
          (mapconcat #'(lambda (s) s) (mapcar #'car (cdr list)) "\n\n")))

(defun mp/sitemap-entry (entry style project)
  "Formatter function for a sitemap entry. ENTRY is the file
name. STYLE is the style of the sitemap (not used here but
mandatory for the function to work with the ox-publish). PROJECT
is the current project."
  (when (not (directory-name-p entry))
    (format (concat
             "-----\n"
             "* [[file:%s][%s]]  %s\n"
             "#+BEGIN_publidate\n"
             "Publié le %s\n"
             "#+END_publidate\n"
             "%s\n"
             "\n")
            entry
            (org-publish-find-title entry project)
            (concat (mapconcat #'(lambda (s) (format ":%s" s)) (mp/get-tags entry) "") ":")
            (capitalize (format-time-string "%d/%m/%Y" (org-publish-find-date entry project)))
            ;; Build the actual preview of the article
            (let ((preview (mp/blog-post-get-preview entry)))
              (format
               (concat
                "%s\n\n"
                "#+BEGIN_readmorelink\n"
                "[[file:%s][Plus \\rarr]]\n"
                "#+END_readmorelink\n")
               preview entry)))))

(setq org-publish-project-alist
      `(("blog"
         :components ("articles" "pages" "static"))
        ("articles"
         :base-directory ,(concat  mp/project-path "blog")
         :base-extension "org"
         :publishing-directory ,(concat mp/project-path "public/blog")
         :publishing-function org-html-publish-to-html
         :with-title t
         :with-author t
         :with-creator nil
         :with-date t
         :headline-level 4
         :with-toc nil
         :with-drawers t
         :with-sub-superscript nil
         :section-numbers nil
         :html-head nil
         :html-head-extra ,mp/head-extra
         :head-include-scripts nil
         :html-viewport nil
         :html-doctype "html5"
         :html-link-up ""
         :html-link-home ""
         :html-preamble mp/header
         :html-postamble ,mp/footer
         :html-footnote-section "<div id='footnotes'><!--%s-->%s</div>"
         :html-format-drawer-function mp/org-export-format-drawer
         :htmlized-source t
         :auto-sitemap t
         :sitemap-function mp/sitemap
         :sitemap-format-entry mp/sitemap-entry
         :sitemap-filename "index.org"
         :sitemap-title "Articles"
         :sitemap-sort-files anti-chronologically)
        ("pages"
         :base-directory ,mp/project-path
         :base-extension "org"
         :publishing-directory ,(concat mp/project-path "public")
         :publishing-function org-html-publish-to-html
         :exclude "README.org"
         :recursive t
         :with-title t
         :with-author nil
         :with-toc nil
         :with-date t
         :with-drawers t
         :with-sub-superscript nil
         :section-numbers nil
         :html-viewport nil
         :html-head nil
         :html-head-extra ,mp/head-extra
         :html-doctype "html5"
         :html-link-up ""
         :html-link-home ""
         :html-preamble mp/header
         :html-postamble ,mp/footer
         :htmlized-source t)
        ("static"
         :base-directory ,(concat mp/project-path "static")
         :base-extension ".*"
         :recursive t
         :publishing-directory ,(concat mp/project-path "public/static")
         :publishing-function org-publish-attachment)))
