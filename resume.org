#+TITLE: CV
#+STARTUP: align shrink

* Compétences

- Python
  - Tryton
  - Flask
- C/C++
- Java
  - Spring
- Javascript/Typescript
  - Angular
- Bash
- Docker
- Environnements GNU/Linux (Debian, Ubuntu, Archlinux)
- Bases de données (MySQL, Postgres, MariaDB, MongoDB, Redis)
- Lisp
- PHP

* Formation

| *Juin 2020* | Formation Java/Angular |                                    |
| *2017-2019* | Master Informatique    | /Université de Bordeaux/           |
| *2014-2017* | Licence Informatique   | /Université de Bordeaux/           |
| *2012-2014* | BTS Informatique       | /Lycée Bertran de Born, Périgueux/ |
| *Anglais*   | Niveau Avancé          |                                    |
| *Espagnol*  | Niveau Intermédiaire   |                                    |

* Experiences professionnelles
|                                 |                       |                                 |            | <40>                                                                                                                               |
| *Février 2024 - Aujourd'hui*    | CDI                   | /KBRW/                          | Bordeaux   | Développeur fullstack *(elixir, javascript, riak, react)*                                                                          |
| *Septembre 2020 - Janvier 2024* | CDI                   | /Coopengo/                      | Bordeaux   | Développeur tryton back *(python, tryton, postgres, bash, javascript, docker)*                                                     |
| *Septembre 2019 - Août 2020*    | CDI                   | /AKKA Technologies, AKKA I&S/   | Mérignac   | Ingénieur d'études et développement. Missions :                                                                                    |
|                                 |                       |                                 |            | - /Thalès/ : Portage d'outils de génération de code qualifiés sous Solaris 8 *(C, perl, ada, UNIX, clearcase, bash)*               |
|                                 |                       |                                 |            | - /Thalès/ : Développement et test sur banc d'essaie d'évolutions pour cockpit d'avions *(csh, asm)*                               |
| *Mars - Août 2019*              | Stage                 | /Université de Bordeaux, LaBRI/ | Talence    | Développement d'un premier démonstrateur pour un projet de drones jouant au volley *(python, pybullet, git)*                       |
| *Mai - Septembre 2018*          | Google Summer of Code | /Projet GNU/                    |            | Modularize Automake and Improve its test suite[fn:1] *(perl, bash, git)*                                                           |
| *Juillet 2016*                  | Stage                 | /Université de Bordeaux, CREMI/ | Talence    | Mise en place d'un dokuwiki et recherche d'une solution d'interfaçage avec le LDAP de l'université *(PHP, Apache2)*                |
| *Février - Avril 2014*          | Stage                 | /Alpha System Internationnal/   | Le Bouscat | Création d'une application mobile de suivi pour les commerciaux *(Java, Android)*                                                  |
| *Février 2013*                  | Stage                 | /CGinfomatique 24/              | Périgueux  | Refonte de l'interface administrateur du site et mise en place d'une nouvelle charte graphique pour la boutique *(HTML, CSS, PHP)* |

[fn:1] Vous pouvez trouver mon projet GSoC [[https://summerofcode.withgoogle.com/projects/#6145437149429760][ici]].

* Divers

** Intérêts

- Logiciels et culture libres
- Sports mécaniques
- Jeux de société
